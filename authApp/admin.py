from django.contrib import admin
from .models.cliente import Cliente
from .models.usuario import Usuario
from .models.producto import Producto
from .models.venta import Venta


admin.site.register(Usuario)
admin.site.register(Cliente)
admin.site.register(Producto)
admin.site.register(Venta)



# Register your models here.
