from django.db import models
from .usuario import Usuario

class Producto(models.Model):
    id_producto = models.AutoField(primary_key =True)
    nombre_prducto = models.CharField('Nombre del producto', max_length=30)
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    categoria = models.CharField('Categoria del producto', max_length=30)
    codigo_categoria = models.PositiveIntegerField(default=0)
    precio_producto = models.PositiveBigIntegerField(default=0)
    inventario_producto = models.PositiveBigIntegerField(default=0)
    descuento_activo = models.BooleanField(default=False)
    descuento_porcentaje = models.IntegerField(default=0)
