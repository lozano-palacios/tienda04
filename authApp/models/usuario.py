from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password


class Admin_usuario(BaseUserManager):

    def creacion_usuario(self,nombre_usuario, contrasena=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not nombre_usuario:
            raise ValueError('Users must have an username')
        usuario = self.model(nombre_usuario=nombre_usuario)
        usuario.set_password(contrasena)
        usuario.save(using=self._db)
        return usuario
    
    def creacion_superusuario(self, nombre_usuario, contrasena):
        """
        Creates and saves a superuser with the given username and password.
        """
        usuario=self.creacion_usuario(
            nombre_usuario=nombre_usuario,
            contrasena=contrasena,
        )
        usuario.is_superusuario = True
        usuario.save(using=self._db)
        return usuario

class Usuario(AbstractBaseUser,PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    nombre_usuario = models.CharField('Nombre_usuario', max_length= 15, unique=True)
    contrasena = models.CharField('Contraseña', max_length=256)
    nombre = models.CharField('Nombre', max_length=30)
    apellido = models.CharField('Apellido', max_length=30)
    email = models.EmailField('Email',max_length=100)

    def guardar(self, **kwargs):
        encriptacion = 'mMUj0DrIK6vgtdIYepkIxN'
        self.contrasena = make_password(self.contrasena, encriptacion)
        super().guardar(**kwargs)
    
    objects = Admin_usuario()
    USERNAME_FIELD = 'nombre_usuario'